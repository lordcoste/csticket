<?php

use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('staff', function($table) {
			$table->increments('id')
				->unsigned();

			$table->string('email', 100)
				->index();

			$table->string('password', 60);
			$table->string('nome', 100);
			$table->string('cognome', 100);

			$table->enum('role', array(
					'staff', 'admin'
				))->default('staff');

			$table->boolean('blocked')->default(false);

			$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('staff');
	}

}