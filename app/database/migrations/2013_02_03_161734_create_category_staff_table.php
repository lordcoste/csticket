<?php

use Illuminate\Database\Migrations\Migration;

class CreateCategoryStaffTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_staff', function($table) {

			$table->integer('category_id')
				->unsigned();

			$table->foreign('category_id')
				->references('id')
				->on('categories')
				->on_delete('cascade')
				->on_update('cascade');

			$table->integer('staff_id')
				->unsigned();

			$table->foreign('staff_id')
				->references('id')
				->on('staff')
				->on_delete('cascade')
				->on_update('cascade');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_staff');
	}

}