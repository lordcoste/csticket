<?php

use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function($table) {
			//$table->increments('id')->unsigned();

			$table->string('token', 10)
				->primary();

			$table->string('author', 100)
				->index();

			$table->integer('staff_id')
				->unsigned()
				->nullable()
				->index();

			$table->integer('category_id')
				->unsigned()
				->index();

			$table->enum('priority', array(
					'low', 'medium', 'high', 'critical'
				))->index();

			$table->string('titolo', 255);

			$table->text('description');

			$table->timestamps();

			$table->foreign('staff_id')
				->references('id')
				->on('staff');

			$table->foreign('category_id')
				->references('id')
				->on('categories');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}