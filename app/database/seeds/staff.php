<?php

return array(

    array(
        'email'      => 'lordcoste@gmail.com',
        'password'   => '$2y$08$zc6Vu9cAnmhnpQOMXfYV7eGnjS9a00zoDxmIn4r6Xia90nmOT4eza',
        'nome'       => 'Stefano',
        'cognome'    => 'Colao',
        'role'       => 'admin',
        'blocked'    => false,
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
    ),

);
