<?php

return array(

    array(
        'id'     => 1,
        'name'   => 'Root',
        'parent' => null,
    ),

    array(
        'id'     => 2,
        'name'   => 'Tecnica',
        'parent' => 1,
    ),

    array(
        'id'     => 3,
        'name'   => 'Vendite',
        'parent' => 1,
    ),

    array(
        'id'     => 4,
        'name'   => 'Database',
        'parent' => 2,
    ),

    array(
        'id'     => 5,
        'name'   => 'FTP',
        'parent' => 2,
    ),

    array(
        'id'     => 6,
        'name'   => 'Confronto piani',
        'parent' => 3,
    ),
);
