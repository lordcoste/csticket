<?php
setlocale(LC_TIME, 'it_IT.UTF-8', 'it_IT.ISO8859-1');

HTML::macro('nav_link', function($url, $text) {
    $class = ( Request::is($url) || Request::is($url.'/*') ) ? ' class="active"' : '';
    return '<li' . $class . '><a href="' . URL::to($url) . '">' . $text . '</a></li>';
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::model('category', 'Category');
//Route::get('category/{category}/edit', 'CategoryController@edit');
//Route::put('category/{category}', 'CategoryController@update');

Route::group(array('before' => 'auth'), function()
{
    Route::group(array('before' => 'admin'), function()
    {
        Route::resource('category', 'CategoryController');
        Route::resource('staff', 'StaffController');

        Route::put('staff/{staff}/block', 'StaffController@block');
        Route::put('staff/{staff}/unblock', 'StaffController@unblock');
    });

    Route::get('logout', function() {
        Auth::logout();
        return Redirect::to('login');
    });
});

Route::get('login', array('as' => 'login', 'before' => 'guest', function() {
    return View::make('login');
}));

Route::post('login', function() {

    $email = Input::get('username');
    $password = Input::get('password');

    if ( Auth::attempt(array('email' => $email, 'password' => $password)) )
    {
        return Redirect::to('category');
    }
    else
    {
        return Redirect::to('login')
            ->with('login_errors', true);
    }
});