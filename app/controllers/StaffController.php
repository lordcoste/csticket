<?php

use \Noherczeg\Breadcrumb\Breadcrumb;

class StaffController extends BaseController {

	/**
	 * Filtro protezione csrf
	 */
	public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$bc = new Breadcrumb(URL::to('/'), 'it');
		$bc->from(array('staff'))->append('Home', 'left', true);

		$membri = StaffMember::getAll();

		return View::make('staff.index', array(
			'page_title'       => 'Staff',
			'page_description' => 'Lista dei membri',
			'breadcrumb'       => $bc->build('bootstrap'),
			'staff'            => $membri
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$bc = new Breadcrumb(URL::to('/'), 'it');
		$bc->from(array('staff', 'create'))->append('Home', 'left', true);

		$categorie = CategoryPresenter::treeAsCheckbox();

		return View::make('staff.create', array(
			'page_title'       => 'Aggiungi utente',
			'page_description' => 'Aggiungi un nuovo membro staff',
			'breadcrumb'       => $bc->build('bootstrap'),
			'categorie'        => $categorie,
		));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$staff = new StaffMember();

		$staff->email                 = Input::get('email');
		$staff->nome                  = Input::get('nome');
		$staff->cognome               = Input::get('cognome');
		$staff->role                  = Input::get('role');
		$staff->password              = Input::get('password');
		$staff->password_confirmation = Input::get('password_confirmation');

		foreach((array)Input::get('categorie') as $id => $name) {
			StaffMember::$rules[] = array('categorie[' . $id . ']' => 'required|numeric|exists:categories,id');
		}

		if($staff->save()) {

			// Associo le categorie all'utente
			$staff->categories()->sync(Input::get('categorie'));

			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'success',
						'text' => 'Utente <b>' . $staff->nome . ' ' . $staff->cognome . '</b> aggiunto con successo.'
					),
				));
		} else {
			return Redirect::action('StaffController@create')
				->with('message', array(
					array(
						'type' => 'error',
						'text' => 'L\'utente non &egrave; stato aggiunto.'
					),
				))
				->withErrors($staff->validationErrors)
				->withInput(array('except' => array('password', 'password_confirmation')));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if ( ! $this->staffMember($id)) {
			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Membro staff inesistente.'
					),
				));
		}

		$bc = new Breadcrumb(URL::to('/'), 'it');
		$bc->from(array('staff', $this->staffMember->nome . ' ' . $this->staffMember->cognome))->append('Home', 'left', true);

		$categorie = CategoryPresenter::treeAsCheckbox(array(), array_pluck($this->staffMember->categories->toArray(), 'id'));

		return View::make('staff.edit', array(
			'page_title'       => 'Membro: ' . $this->staffMember->nome . ' ' . $this->staffMember->cognome,
			'page_description' => 'Modifica profilo membro staff',
			'breadcrumb'       => $bc->build('bootstrap'),
			'staff'            => $this->staffMember,
			'categorie'        => $categorie,
		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update($id)
	{
		if ( ! $this->staffMember($id)) {
			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Membro staff inesistente.'
					),
				));
		}

		$this->staffMember->email   = Input::get('email');
		$this->staffMember->nome    = Input::get('nome');
		$this->staffMember->cognome = Input::get('cognome');

		StaffMember::$rules['email'] .= ',' . $id;

		$password = Input::get('password', false);

		// Se la password non viene modificata non serve confermarla
		if ( ! $password) {
			$this->staffMember->autoHashPasswordAttributes = false;
			$this->staffMember->password_confirmation = $this->staffMember->password;
		} else {
			$this->staffMember->password = $password;
			$this->staffMember->password_confirmation = Input::get('password_confirmation');
		}

		foreach((array)Input::get('categorie') as $id => $nome) {
			StaffMember::$rules[] = array('categorie[' . $id . ']' => 'required|numeric|exists:categories,id');
		}

		if($this->staffMember->save()) {
			// Associo le categorie all'utente
			$this->staffMember->categories()->sync(Input::get('categorie'));

			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'success',
						'text' => 'Profilo di <b>' . $this->staffMember->nome . ' ' . $this->staffMember->cognome . '</b> aggiornato con successo.'
					),
				));
		} else {
			return Redirect::action('StaffController@edit', array($this->staffMember->id))
				->with('message', array(
					array(
						'type' => 'error',
						'text' => 'Profilo di <b>' . $this->staffMember->nome . ' ' . $this->staffMember->cognome . '</b> non aggiornato.'
					),
				))
				->withErrors($this->staffMember->validationErrors)
				->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Blocca l'account
	 *
	 * @return Response
	 */
	public function block($id)
	{
		if ( ! $this->staffMember($id)) {
			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Membro staff inesistente.'
					),
				));
		}

		if (Auth::user()->id == $id) {
			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Non puoi bloccare il tuo stesso account.'
					),
				));
		}

		DB::table('staff')
            ->where('id', $id)
            ->update(array('blocked' => true));

		return Redirect::action('StaffController@index')
			->with('message', array(
				array(
					'type' => 'success',
					'text' => 'Account di <b>' . $this->staffMember->nome . ' ' . $this->staffMember->cognome . '</b> bloccato con successo.'
				),
			));
	}

	/**
	 * Sblocca l'account
	 *
	 * @return Response
	 */
	public function unblock($id)
	{
		if ( ! $this->staffMember($id)) {
			return Redirect::action('StaffController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Membro staff inesistente.'
					),
				));
		}

		DB::table('staff')
            ->where('id', $id)
            ->update(array('blocked' => false));

		return Redirect::action('StaffController@index')
			->with('message', array(
				array(
					'type' => 'success',
					'text' => 'Account di <b>' . $this->staffMember->nome . ' ' . $this->staffMember->cognome . '</b> sbloccato con successo.'
				),
			));
	}

	/**
	 * Setta e restituisce lo StaffMember corrente
	 *
	 * @param  int $id
	 * @return StaffMember
	 */
	protected function staffMember($id)
    {
        $this->staffMember = staffMember::find($id);
        return $this->staffMember;
    }

}