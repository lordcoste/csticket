<?php

use \Noherczeg\Breadcrumb\Breadcrumb;

class CategoryController extends BaseController {

	/**
	 * Filtro protezione csrf
	 */
	public function __construct()
    {
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$bc = new Breadcrumb(URL::to('/'), 'it');
		$bc->from(array('category'))->append('Home', 'left', true);

		$categorie = CategoryPresenter::treeAsTable();
		$select = CategoryPresenter::treeAsSelect(array(
			'id'    => 'parent',
			'name'  => 'parent',
			'class' => 'uniformselect',
		));

		return View::make('category.index', array(
			'page_title'       => 'Categorie',
			'page_description' => 'Lista delle categorie',
			'breadcrumb'       => $bc->build('bootstrap'),
			'categorie'        => $categorie,
			'select'           => $select
		));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$categoria = new Category();

		$categoria->name = Input::get('name');
		$categoria->parent = Input::get('parent');

		if($categoria->save()) {
			return Redirect::action('CategoryController@index')
				->with('message', array(
					array(
						'type' => 'success',
						'text' => 'Categoria aggiunta con successo.'
					),
				));
		} else {
			return Redirect::action('CategoryController@index')
				->with('message', array(
					array(
						'type' => 'error',
						'text' => 'La categoria non &egrave; stata aggiunta.'
					),
				))
				->withErrors($categoria->validationErrors)
				->withInput();
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if ( ! $this->category($id)) {
			return Redirect::action('CategoryController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Categoria inesistente.'
					),
				));
		}

		$bc = new Breadcrumb(URL::to('/'), 'it');
		$bc->from(array('category', $this->category->name))->append('Home', 'left', true);

		$select = CategoryPresenter::treeAsSelect(array(
			'id'    => 'parent',
			'name'  => 'parent',
			'class' => 'uniformselect',
		), $this->category->parent);

		return View::make('category.edit', array(
			'page_title'       => 'Categoria: ' . $this->category->name,
			'page_description' => 'Modifica categoria',
			'breadcrumb'       => $bc->build('bootstrap'),
			'categoria'        => $this->category,
			'select'           => $select,
		));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update($id)
	{
		if ( ! $this->category($id)) {
			return Redirect::action('CategoryController@index')
				->with('message', array(
					array(
						'type' => 'warning',
						'text' => 'Categoria inesistente.'
					),
				));
		}

		$this->category->name = Input::get('name');
		$this->category->parent = Input::get('parent');

		foreach((array)Input::get('associati') as $id => $staff) {
			Category::$rules[] = array('associati[' . $id . ']' => 'required|numeric|exists:staff,id');
		}

		if($this->category->save()) {

			// Associo gli utenti alla categoria
			$this->category->staff_members()->sync(Input::get('associati'));

			return Redirect::action('CategoryController@index')
				->with('message', array(
					array(
						'type' => 'success',
						'text' => 'La categoria <b>' . $this->category->name . '</b> aggiornata con successo.'
					),
				));
		} else {
			return Redirect::action('CategoryController@edit', array($this->category->id))
				->with('message', array(
					array(
						'type' => 'error',
						'text' => 'La categoria <b>' . $this->category->name . '</b> non &egrave; stata aggiornata.'
					),
				))
				->withErrors($this->category->validationErrors)
				->withInput();
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	/**
	 * Setta e restituisce la categoria corrente
	 *
	 * @param  int $id
	 * @return Category
	 */
	protected function category($id)
    {
        $this->category = Category::find($id);
        return $this->category;
    }

}