<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;
use BigElephant\Presenter\PresentableInterface;

class StaffMember extends Ardent implements PresentableInterface, UserInterface, RemindableInterface {

	/**
	 * Nome della tabella usata da questo modello.
	 *
	 * @var string
	 */
	protected $table = 'staff';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

    /**
     * Rimuove in automatico gli attributi utili
     * solo alla validazione e non al modello
     *
     * @var boolean
     */
    public $autoPurgeRedundantAttributes = true;

    /**
     * Attributi di cui eseguire l'hash automatico
     *
     * @var array
     */
    public static $passwordAttributes = array('password');

    /**
     * Eseguire l'hash automatico degli attributi
     *
     * @var boolean
     */
    public $autoHashPasswordAttributes = true;

    /**
    * Regole di validazione
    *
    * @var array
    */
    public static $rules = array(
        'email'     => 'required|email|unique:staff,email',
        'role'      => 'required|in:admin,staff',
        'nome'      => 'required|between:1,100',
        'cognome'   => 'required|between:1,100',
        'password'  => 'required|confirmed',
    );

    /**
     * Elenco dei ruoli utente
     *
     * @var array
     */
    public static $roles = array(
        'admin' => 'Amministratore',
        'staff' => 'Membro staff',
    );

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Relazione molti-molti con le categorie
	 *
	 * @return Illuminate\Database\Eloquent\Collection
	 */
	public function categories()
    {
        return $this->belongsToMany('Category', 'category_staff', 'staff_id');
    }

    /**
     * Ritorna l'istanza del Presenter
     *
     * @return StaffPresenter
     */
    public function getPresenter()
    {
        return new StaffPresenter($this);
    }

    public static function getAll()
    {
    	return static::all();
    }

}