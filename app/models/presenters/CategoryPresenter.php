<?php

use BigElephant\Presenter\Presenter;

class CategoryPresenter extends Presenter {

    /**
     * Rappresenta l'albero della categorie come
     * una lista HTML (ul>li)
     *
     * @param  array       $attributes attributi HTML
     * @param  array       $tree
     * @param  integer     $deep
     * @return string
     */
    public static function treeAsList(array $attributes = array(), $tree = null, $deep = 0)
    {
        $html = '';

        if(is_null($tree)) {
            $tree = Category::tree();
        }

        if($deep == 0) {
            $html .= '<ul' . HTML::attributes($attributes) . '>' . PHP_EOL;
        } else {
            $html .= '<ul>' . PHP_EOL;
        }

        foreach($tree as $node) {

            $html .= '<li>' . PHP_EOL;

            $html .= $node['name'] . PHP_EOL;

            $html .= static::treeAsList(array(), $node['children'], $deep+1) . PHP_EOL;

            $html .= '</li>' . PHP_EOL;
        }

        $html .= '</ul>' . PHP_EOL;

        return $html;
    }

    /**
     * Rappresenta l'albero della categorie come
     * una modulo di scelta HTML (select>option)
     *
     * @param  array   $attributes  [Attributi html]
     * @param  [type]  $parent
     * @param  [type]  $tree        [Collection du categorie]
     * @param  integer $deep
     * @return string
     */
    public static function treeAsSelect(array $attributes = array(), $parent = null, $tree = null, $deep = 0)
    {
        $html = '';

        if(is_null($tree)) {
            $tree = Category::tree();
        }

        if($deep == 0) {
            $html .= '<select ' . HTML::attributes($attributes) . '>' . PHP_EOL;
            $html .= '<option value="1"></option>' . PHP_EOL;
        }

        foreach($tree as $node) {

            $selected = '';
            if($parent == $node['id']) {

                $selected = 'selected="selected"';
            }

            $html .= '<option value="' . $node['id'] . '" ' . $selected . '>' . str_repeat('-', $deep);

            $html .= $node['name'] . '</option>' . PHP_EOL;

            $html .= static::treeAsSelect(array(), $parent, $node['children'], $deep+1);
        }

        if($deep == 0) {
            $html .= '</select>' . PHP_EOL;
        }

        return $html;
    }

    public static function treeAsTable($tree = null, $deep = 0)
    {
        $categorie = array();

        if(is_null($tree)) {
            $tree = Category::tree();
        }

        foreach($tree as $node) {
            $categorie[] = array(
                'id' => $node['id'],
                'name' => str_repeat('&nbsp;', $deep*5) . $node['name'],
            );

            $categorie = array_merge($categorie, static::treeAsTable($node['children'], $deep+1));
        }

        return $categorie;
    }

    public static function treeAsCheckbox(array $attributes = array(), array $selected = array(), $tree = null, $deep = 0)
    {
        $html = '';

        if(is_null($tree)) {
            $tree = Category::tree();
        }

        if($deep == 0) {
            $html .= '<span class="formwrapper" style="margin-left:120px">' . PHP_EOL;
        }

        foreach($tree as $node) {

            $checked = false;
            $old = Input::old('categorie');

            if(isset($old[$node['id']]) || in_array($node['id'], $selected)) {
                $checked = true;
            }

            $html .= Form::checkbox('categorie[]', $node['id'], $checked, $attributes) . str_repeat('-', $deep) . $node['name'] . '<br />' . PHP_EOL;

            $html .= static::treeAsCheckbox($attributes, $selected, $node['children'], $deep+1);
        }

        if($deep == 0) {
            $html .= '</span>' . PHP_EOL;
        }

        return $html;
    }
}
