<?php

use BigElephant\Presenter\Presenter;

class StaffPresenter extends Presenter {

    /**
     * Ristituisce l'html del link
     * blocca/sblocca utente
     *
     * @return string
     */
    public function getBlockUnblockButton()
    {
        if($this->blocked)
        {
            return '<a href="' . URL::action('StaffController@unblock', array($this->id)) . '" data-method="PUT" class="btn btn-success btn-rounded"><i class=" icon-remove-sign icon-white"></i> Sblocca</a>';
        }
        else
        {
            return '<a href="' . URL::action('StaffController@block', array($this->id)) . '" data-method="PUT" class="btn btn-danger btn-rounded"><i class=" icon-remove-sign icon-white"></i> Blocca</a>';
        }
    }
}
