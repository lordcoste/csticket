<?php

use BigElephant\Presenter\PresentableInterface;
use LaravelBook\Ardent\Ardent;

class Category extends Ardent implements PresentableInterface {

    /**
     * Nome della tabella usata da questo modello.
     */
    protected $table = 'categories';

    /**
     * Memorizzare timestap created_at e updated_at
     */
    public $timestamps = false;

    /**
    * Regole di validazione
    */
    public static $rules = array(
        'name'   => 'required|between:1,100', //|unique:categories,name',
        'parent' => 'required|integer|exists:categories,id',
    );

    /**
     * Relazione uno-uno con altra Categoria
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function parent()
    {
        return $this->hasOne('Category', 'id', 'parent');
    }

    /**
     * Relazione uno-molti con altre Categorie
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function children()
    {
        return $this->hasMany('Category', 'parent', 'id');
    }

    /**
     * Preleva la gerarchia delle categorie fino ad una certa profondità
     * Il risultato è salvato in cache
     *
     * @param  integer $deep
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function tree($deep = 10)
    {
        $tree = Cache::rememberForever('category_tree', function() use ($deep)
        {
            return static::with(implode('.', array_fill(0, $deep, 'children')))
                ->where('parent', 1)->get()->toArray();
        });

        return $tree;
    }

    /**
     * Relazione molti-molti con StaffMember
     *
     * @return Collection
     */
    public function staff_members()
    {
        return $this->belongsToMany('StaffMember', 'category_staff', 'category_id', 'staff_id');
    }

    /**
     * Ritorna l'istanza del Presenter
     *
     * @return CategoryPresenter
     */
    public function getPresenter()
    {
        return new CategoryPresenter($this);
    }

    /**
     * Forget cache when saved
     */
    public function afterSave( $success )
    {
        if( $success ) {
            Cache::forget('category_tree');
        }
    }

    /**
     * Forget cache when deleted
     */
    public function delete()
    {
        parent::delete();
        Cache::forget('category_tree');
    }

    public function getNotAssociated()
    {
        return DB::table('staff')
            ->whereNotExists(function($query)
            {
                $query->select('staff_id')
                      ->from('category_staff')
                      ->where('category_id', '=', $this->id)
                      ->whereRaw('staff.id = staff_id');
            })
            ->get();
    }
}