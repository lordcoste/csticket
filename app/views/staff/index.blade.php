@extends('layouts.default')

@section('content')
    <ul class="msghead_menu">
        <li>
            <a href="{{ URL::action('StaffController@create') }}" class="btn"><span class="icon-plus"></span> Aggiungi</a>
        </li>
        <li class="right btn-group">
            <a class="btn prev prev_disabled"><span class="icon-chevron-left"></span></a>
            <a class="btn next"><span class="icon-chevron-right"></span></a>
        </li>
        <li class="right"><span class="pageinfo">1-10 of 2,139</span></li>
    </ul>

    <br />

    <table class="table table-bordered table-striped" style="margin-top:20px">

        <tr>
            <th>Email</th>
            <th>Ruolo</th>
            <th>Cognome</th>
            <th>Nome</th>
            <th>Login</th>
            <th></th>
            <th></th>
        </tr>

        @foreach ($staff as $s)
            <tr>
                <td>{{{ HTML::to('staff/' . $s->id . '/edit', $s->email) }}}</td>
                <td>{{ $s->role }}</td>
                <td>{{ $s->cognome }}</td>
                <td>{{ $s->nome }}</td>
                <td>Login</td>
                <td>{{{ $s->getBlockUnblockButton() }}}</td>
                <td class="centeralign"><a href="#" class="deleterow"><span class="icon-trash"></span></a></td>
            </tr>
        @endforeach

    </table>
@stop