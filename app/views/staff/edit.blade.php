@extends('layouts.default')

@section('content')
<div class="row-fluid">

    <div class="span12">
        {{{ Form::open('staff/' . $staff->id, 'PUT', array('class' => 'stdform editprofileform')) }}}
            {{{ Form::token() }}}
            <h4>Informazioni di Login</h4>
            <p class="control-group @if ($errors->has('email')) error @endif">
                <label>Email:</label>
                <input type="text" name="email" class="input-xlarge" value="{{ Input::old('email', $staff->email)}}" placeholder="you@yourdomain.com">
                @if ($errors->has('email'))
                    {{{ $errors->first('email') }}}
                @endif
            </p>
            <p class="control-group @if ($errors->has('password')) error @endif">
                <label>Password:</label>
                <input type="password" name="password" class="input-xlarge" value="" placeholder="password">
                @if ($errors->has('password'))
                    {{{ $errors->first('password') }}}
                @endif
            </p>
            <p class="control-group @if ($errors->has('password_confirmation')) error @endif">
                <label>Conferma:</label>
                <input type="password" name="password_confirmation" class="input-xlarge" value="" placeholder="conferma">
                @if ($errors->has('password_confirmation'))
                    {{{ $errors->first('password_confirmation') }}}
                @endif
            </p>

            <br>

            <h4>Altre Informazioni</h4>
            <p class="control-group @if ($errors->has('nome')) error @endif">
                <label>Nome:</label>
                <input type="text" name="nome" class="input-xlarge" value="{{ Input::old('nome', $staff->nome)}}" placeholder="Nome">
                @if ($errors->has('nome'))
                    {{{ $errors->first('nome') }}}
                @endif
            </p>
            <p class="control-group @if ($errors->has('cognome')) error @endif">
                <label>Cognome:</label>
                <input type="text" name="cognome" class="input-xlarge" value="{{ Input::old('cognome', $staff->cognome)}}" placeholder="Cognome">
                @if ($errors->has('cognome'))
                    {{{ $errors->first('cognome') }}}
                @endif
            </p>
            <p class="control-group @if ($errors->has('role')) error @endif">
                <label>Ruolo:</label>
                {{{ Form::select('role', StaffMember::$roles, Input::old('role', $staff->role), array('class' => 'uniformselect')) }}}
                @if ($errors->has('role'))
                    {{{ $errors->first('role') }}}
                @endif
            </p>

            <br>

            <h4>Categorie</h4>
            <p>
                <label>Associate:</label>
                {{{ $categorie }}}
            </p>

            <br>
            <p>
                <button type="submit" class="btn btn-primary">Salva</button>
            </p>
        {{{ Form::close() }}}
    </div><!--span12-->
</div>
@stop