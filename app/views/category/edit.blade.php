@extends('layouts.default')

@section('content')
    <h4 class="widgettitle nomargin shadowed">Modifica categoria</h4>
    <div class="widgetcontent bordered shadowed nopadding">
        {{{ Form::open('category/' . $categoria->id, 'PUT', array('class' => 'stdform stdform2 dualselect_form')) }}}
            {{{ Form::token() }}}
            <p class="control-group @if ($errors->has('name')) error @endif">
                <label>Nome</label>
                <span class="field">
                    <input type="text" name="name" id="name" class="input-large" value="{{ Input::old('name', $categoria->name) }}">
                    @if ($errors->has('name'))
                       {{{ $errors->first('name') }}}
                    @endif
                </span>
            </p>

            <p class="control-group @if ($errors->has('parent')) error @endif">
                <label>Padre</label>
                <span class="field">
                    {{{ $select }}}
                    @if ($errors->has('parent'))
                        {{{ $errors->first('parent') }}}
                    @endif
                </span>
            </p>

            <p>
                <label>Utenti / Associati</label>
                <span id="dualselect" class="dualselect">
                    <select class="uniformselect" name="select3" multiple="multiple" size="10">
                        @foreach ($categoria->getNotAssociated() as $s)
                            <option value="{{ $s->id }}">{{ $s->cognome }}</option>
                        @endforeach
                    </select>
                    <span class="ds_arrow">
                        <button class="btn ds_prev"><i class="icon-chevron-left"></i></button><br />
                        <button class="btn ds_next"><i class="icon-chevron-right"></i></button>
                    </span>
                    <select name="associati[]" multiple="multiple" size="10">
                        @foreach ($categoria->staff_members as $s)
                            <option value="{{ $s->id }}">{{ $s->cognome }}</option>
                        @endforeach
                    </select>
                </span>
            </p>

            <p class="stdformbutton">
                <button type="submit" class="btn btn-primary">Salva</button>
            </p>

        {{{ Form::close() }}}
    </div>
    <script type="text/javascript">
    jQuery(document).ready(function(){
        // Dual Box Select
        var db = jQuery('#dualselect').find('.ds_arrow button');    //get arrows of dual select
        var sel1 = jQuery('#dualselect select:first-child');        //get first select element
        var sel2 = jQuery('#dualselect select:last-child');         //get second select element

        //sel2.empty(); //empty it first from dom.

        db.click(function(){
            var t = (jQuery(this).hasClass('ds_prev'))? 0 : 1;  // 0 if arrow prev otherwise arrow next
            if(t) {
                sel1.find('option').each(function(){
                    if(jQuery(this).is(':selected')) {
                        jQuery(this).attr('selected',false);
                        var op = sel2.find('option:first-child');
                        sel2.append(jQuery(this));
                    }
                });
            } else {
                sel2.find('option').each(function(){
                    if(jQuery(this).is(':selected')) {
                        jQuery(this).attr('selected',false);
                        sel1.append(jQuery(this));
                    }
                });
            }
            return false;
        });

        jQuery('.dualselect_form').submit(function(){
            jQuery('option', sel2).attr('selected', 'selected');
        });
    });
    </script>
@stop