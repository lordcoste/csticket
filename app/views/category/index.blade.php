@extends('layouts.default')

@section('content')
    <table class="table table-bordered table-striped">

        <tr>
            <th>Categoria</th>
            <th></th>
        </tr>

        @foreach ($categorie as $c)
            <tr>
                <td>{{{ HTML::to('category/' . $c['id'] . '/edit', $c['name']) }}}</td>
                <td class="centeralign"><a href="#" class="deleterow"><span class="icon-trash"></span></a></td>
            </tr>
        @endforeach

    </table>

    <br />

    <h4 class="widgettitle nomargin shadowed">Aggiungi categoria</h4>
    <div class="widgetcontent bordered shadowed nopadding">
        {{{ Form::open('category', 'POST', array('class' => 'stdform stdform2')) }}}
            {{{ Form::token() }}}
            <p class="control-group @if ($errors->has('name')) error @endif">
                <label>Nome</label>
                <span class="field">
                    <input type="text" name="name" id="name" class="input-large" value="{{ Input::old('name') }}">
                    @if ($errors->has('name'))
                        {{{ $errors->first('name') }}}
                    @endif
                </span>
            </p>

            <p class="control-group @if ($errors->has('parent')) error @endif">
                <label>Padre</label>
                <span class="field">
                    {{{ $select }}}
                    @if ($errors->has('parent'))
                        {{{ $errors->first('parent') }}}
                    @endif
                </span>
            </p>

            <p class="stdformbutton">
                <button type="submit" class="btn btn-primary">Aggiungi</button>
            </p>
        {{{ Form::close() }}}
    </div>
@stop