<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>{{ $page_title . ' - ' ?: '' }}csTicket</title>
    <link rel="stylesheet" href="{{ URL::asset('css/style.default.css') }}">

    <script src="{{ URL::asset('js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.uniform.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('js/custom.js') }}"></script>
</head>

<body>

<div class="mainwrapper">

    <!-- START OF LEFT PANEL -->
    <div class="leftpanel">

        <div class="logopanel">
            <h1><a href="{{ URL::to('/') }}">csTicket <span>v1.0</span></a></h1>
        </div><!--logopanel-->

        <div class="datewidget">Oggi &egrave; {{ strftime('%A, %b %e, %Y %I:%M%p') }}</div>

        <div class="searchwidget">
            <form action="http://themepixels.com/main/themes/demo/webpage/katniss/results.html" method="post">
                <div class="input-append">
                    <input type="text" class="span2 search-query" placeholder="Search here...">
                    <button type="submit" class="btn"><span class="icon-search"></span></button>
                </div>
            </form>
        </div><!--searchwidget-->

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Menu</li>
                @if (Auth::user()->role == 'admin')
                    {{{ HTML::nav_link('category', '<span class="icon-align-justify"></span> Categorie') }}}
                    {{{ HTML::nav_link('staff', '<span class="icon-user"></span> Utenti') }}}
                @endif
                <li class="dropdown"><a href="#"><span class="icon-briefcase"></span> UI Elements &amp; Widgets</a>
                    <ul>
                        <li><a href="elements.html">Theme Components</a></li>
                        <li><a href="bootstrap.html">Bootstrap Components</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href="#"><span class="icon-th-list"></span> Tables</a>
                    <ul>
                        <li><a href="table-static.html">Static Table</a></li>
                        <li><a href="table-dynamic.html">Dynamic Table</a></li>
                    </ul>
                </li>
                <li><a href="typography.html"><span class="icon-font"></span> Typography</a></li>
                <li><a href="charts.html"><span class="icon-signal"></span> Graph &amp; Charts</a></li>
                <li><a href="messages.html"><span class="icon-envelope"></span> Messages</a></li>
                <li><a href="buttons.html"><span class="icon-hand-up"></span> Buttons &amp; Icons</a></li>
                <li class="dropdown"><a href="#"><span class="icon-pencil"></span> Forms</a>
                    <ul style="display: block;">
                        <li><a href="forms.html">Form Styles</a></li>
                        <li><a href="wizards.html">Wizard Form</a></li>
                        <li><a href="wysiwyg.html">WYSIWYG</a></li>
                    </ul>
                </li>
                <li><a href="calendar.html"><span class="icon-calendar"></span> Calendar</a></li>
                <li><a href="animations.html"><span class="icon-play"></span> Animations</a></li>
                <li class="dropdown"><a href="#"><span class="icon-book"></span> Other Pages</a>
                    <ul>
                        <li><a href="404.html">404 Error Page</a></li>
                        <li><a href="invoice.html">Invoice Page</a></li>
                        <li><a href="editprofile.html">Edit Profile</a></li>
                        <li><a href="grid.html">Grid Styles</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--leftmenu-->

    </div><!--mainleft-->
    <!-- END OF LEFT PANEL -->

    <!-- START OF RIGHT PANEL -->
    <div class="rightpanel">
        <div class="headerpanel">
            <a href="#" class="showmenu"></a>

            <div class="headerright">
                <div class="dropdown notification">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="http://themepixels.com/page.html">
                        <span class="iconsweets-globe iconsweets-white"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="nav-header">Notifications</li>
                        <li>
                            <a href="#">
                            <strong>3 people viewed your profile</strong><br />
                            <img src="img/thumbs/thumb1.png" alt="" />
                            <img src="img/thumbs/thumb2.png" alt="" />
                            <img src="img/thumbs/thumb3.png" alt="" />
                            </a>
                        </li>
                        <li><a href="#"><span class="icon-envelope"></span> New message from <strong>Jack</strong> <small class="muted"> - 19 hours ago</small></a></li>
                        <li><a href="#"><span class="icon-envelope"></span> New message from <strong>Daniel</strong> <small class="muted"> - 2 days ago</small></a></li>
                        <li><a href="#"><span class="icon-user"></span> <strong>Bruce</strong> is now following you <small class="muted"> - 2 days ago</small></a></li>
                        <li class="viewmore"><a href="#">View More Notifications</a></li>
                    </ul>
                </div><!--dropdown-->

                <div class="dropdown userinfo">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#">Salve, {{Auth::user()->nome}}! <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="editprofile.html"><span class="icon-edit"></span> Edit Profile</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><span class="icon-wrench"></span> Account Settings</a></li>
                        <li><a href="#"><span class="icon-eye-open"></span> Privacy Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="{{{ URL::to('logout') }}}"><span class="icon-off"></span> Logout</a></li>
                    </ul>
                </div><!--dropdown-->

            </div><!--headerright-->

        </div><!--headerpanel-->

        <div class="breadcrumbwidget">
            <ul class="skins">
                <li class="fixed selected"><a href="#" class="skin-layout fixed"></a></li>
                <li class="wide"><a href="#" class="skin-layout wide"></a></li>
            </ul><!--skins-->

            {{{ $breadcrumb }}}
        </div><!--breadcrumbs-->

        <div class="pagetitle">
            <h1>{{ $page_title }}</h1> <span>{{ $page_description }}</span>
        </div><!--pagetitle-->

        <div class="maincontent">
            <div class="contentinner">

                @foreach (Session::get('message', array()) as $m)
                <div class="alert alert-{{ $m['type'] }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {{{ $m['text'] }}}
                </div>
                @endforeach

                @yield('content')
            </div><!--contentinner-->
        </div><!--maincontent-->

    </div><!--mainright-->
    <!-- END OF RIGHT PANEL -->

    <div class="clearfix"></div>

    <div class="footer">
        <div class="footerleft">csTicket v1.0</div>
        <div class="footerright">&copy; 2013 <a href="mailto:s.colao@campus.unimib.it">Colao Stefano</a></div>
    </div><!--footer-->


</div><!--mainwrapper-->
</body>
</html>